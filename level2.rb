require 'open-uri'

File.open("heart-post-urls.txt").each do |line|
  line.chomp!
  puts line
  `sleep 2`
  begin
    content = open("http://www.medhelp.org#{line}").read
    out = File.open("posts/#{line.gsub(/\//, '_')}", 'w')
    out << content
  rescue
    puts "Error opening #{line}"
  end
end
