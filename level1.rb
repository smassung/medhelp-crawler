require 'open-uri'

File.open("heart-toplevel.txt").each do |line|
  line.chomp!
  baseurl = line.split[0]
  max_num = line.split[1].to_i
  for id in 1..max_num
    puts "Crawling #{baseurl} page #{id}"
    `sleep 2`
    begin
      url = "#{baseurl}?page=#{id}"
      content = open(url).read
      filename = baseurl.gsub(/\//, "_")
      out = File.open("crawled/#{filename}.#{id}", 'w')
      out << content
    rescue
      puts "Error opening #{baseurl}"
    end
  end
end
