require 'nokogiri'

def get_author_info(noko)
  authors, are_profs, reply_to = [], [], []
  idx = 0
  for author in noko.xpath('//div[starts-with(@class, "user_info")]')
    if author.xpath('.//div/span//a/text()').size > 0 and idx == 0
      authors << author.xpath('.//div/span//a/text()').text.gsub(/ Comment.*/, "")
      idx += 1
      are_profs << author.xpath('.//div/span//img').size
      reply_to << "NA"
    end
    if author.xpath('.//div/span//a/text()').size > 0 and idx == 1
      idx += 1
      next
    end
    if author.xpath('.//div[@class="question_by"]/span//a/text()').size > 0 and idx > 1
      authors << author.xpath('.//div[@class="question_by"]/span//a/text()')[0]
      idx += 1
      are_profs << author.xpath('.//div[@class="question_by"]/span//img').size
    end
    reply_person = "NA"
    reply_len = author.xpath('.//div[@class="post_question_forum_to"]//a/text()').size
    if reply_len > 0
      reply_person = author.xpath('.//div[@class="post_question_forum_to"]//a/text()')[0].text
    end
    reply_to << reply_person.strip
  end
  [authors, are_profs, reply_to]
end

def get_dates(noko)
  post_dates = noko.xpath('//div[starts-with(@class, "user_info")]//div/text()')
  dates = []
  for date in post_dates
    date = date.text.strip
    dates << date if date.match(/[a-zA-Z]+ [0-9][0-9], [12][0-9][0-9][0-9]/)
  end
  dates
end

def get_posts(noko)
  posts = []
  for content in noko.xpath('//div[@class="KonaBody"]')
    content = content.xpath('.//text()')
    content = content.to_a.join(" ").gsub(/[\r\n]/, " ")
    posts << content.gsub(/[\t ]+/, " ")
  end
  posts
end

def process(noko, output, post_id, file)
  dates = get_dates(noko)
  authors, are_profs, reply_to = get_author_info(noko)
  posts = get_posts(noko)
  order = 1
  title = noko.xpath('//div[@class="ss_header"]/div[@class="desc"]/@title').text
  for user_id, is_pro, content, rep_author, date in authors.zip(are_profs, posts, reply_to, dates)
    output << "#{post_id}\t#{order}\t#{user_id}\t#{file.gsub(/_/, '/')}\t"
    output << "#{is_pro}\t#{content}\t#{rep_author}\t#{title}\t#{date}\n"
    order += 1
  end
end

def main()
  output = File.open("output.tsv", 'w')
  post_id = 0
  Dir.foreach("posts/") do |file|
    file.chomp!
    next if file == "." or file == ".."
    content = File.open("posts/#{file}").read
    noko = Nokogiri::HTML(content)
    process(noko, output, post_id, file)
    post_id += 1
  end
end

main()
